package met.service;

import com.google.cloud.bigquery.*;
import met.util.JsonHelper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class MetBigQueryServiceImpl implements MetBigQueryService {


    public String getListFromMet(String count) throws Exception {
        BigQuery bigquery = BigQueryOptions.getDefaultInstance().getService();

        QueryJobConfiguration queryConfig =
                QueryJobConfiguration.newBuilder(
                        "SELECT "
                                + " 'metmuseum' username, 'USA' country, 'New York' city, link_resource image, title as description, link_resource cover, artist_display_name as first_name  "
                                + "FROM `bigquery-public-data.the_met.objects` "
                                + "where title is not null "
                                + "LIMIT " + count)
                        .setUseLegacySql(false)
                        .build();

        // Create a job ID so that we can safely retry.
        JobId jobId = JobId.of(UUID.randomUUID().toString());
        Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());

        // Wait for the query to complete.
        try {
            queryJob = queryJob.waitFor();
        } catch (Exception e) {
            throw new RuntimeException("Error in querying job : " + e.toString());
        }

        // Check for errors
        if (queryJob == null) {
            throw new RuntimeException("Job no longer exists");
        } else if (queryJob.getStatus().getError() != null) {
            throw new RuntimeException(queryJob.getStatus().getError().toString());
        }

        // Get the results.
        //QueryResponse response = bigquery.getQueryResults(jobId);

        TableResult result = null;
        try {
            result = queryJob.getQueryResults();
        } catch (Exception e) {
            throw new RuntimeException("Error in getting results: " + e.toString());
        }

        JSONArray array = new JSONArray();
        for (FieldValueList row : result.iterateAll()) {
            array.put(new JSONObject()
                    .put("first_name", JsonHelper.replaceNullWithEmptyString(row.get("first_name")))
                    .put("cover", JsonHelper.replaceNullWithEmptyString(row.get("cover")))
                    .put("description", JsonHelper.replaceNullWithEmptyString(row.get("description")))
                    .put("image", JsonHelper.replaceNullWithEmptyString(row.get("image")))
                    .put("city", JsonHelper.replaceNullWithEmptyString(row.get("city")))
                    .put("country", JsonHelper.replaceNullWithEmptyString(row.get("country")))
                    .put("username", JsonHelper.replaceNullWithEmptyString(row.get("username")))
            );
        }
        return array.toString();
    }

	public String getCustomList(String count) throws Exception {
        BigQuery bigquery = BigQueryOptions.getDefaultInstance().getService();

        QueryJobConfiguration queryConfig =
                QueryJobConfiguration.newBuilder(
                        "SELECT "
                                + " object_id, is_highlight, title, artist_display_name, link_resource "
                                + "FROM `bigquery-public-data.the_met.objects` "
                                + "where title is not null "
                                + "LIMIT " + count)
                        .setUseLegacySql(false)
                        .build();

        // Create a job ID so that we can safely retry.
        JobId jobId = JobId.of(UUID.randomUUID().toString());
        Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());

        // Wait for the query to complete.
        try {
            queryJob = queryJob.waitFor();
        } catch (Exception e) {
            throw new RuntimeException("Error in querying job : " + e.toString());
        }

        // Check for errors
        if (queryJob == null) {
            throw new RuntimeException("Job no longer exists");
        } else if (queryJob.getStatus().getError() != null) {
            throw new RuntimeException(queryJob.getStatus().getError().toString());
        }

        // Get the results.
        //QueryResponse response = bigquery.getQueryResults(jobId);

        TableResult result = null;
        try {
            result = queryJob.getQueryResults();
        } catch (Exception e) {
            throw new RuntimeException("Error in getting results: " + e.toString());
        }

        JSONArray array = new JSONArray();
        for (FieldValueList row : result.iterateAll()) {
            array.put(new JSONObject()
                    .put("object_id", JsonHelper.replaceNullWithEmptyString(row.get("object_id")))
                    .put("is_highlight", JsonHelper.replaceNullWithEmptyString(row.get("is_highlight")))
                    .put("title", JsonHelper.replaceNullWithEmptyString(row.get("title")))
                    .put("artist_display_name", JsonHelper.replaceNullWithEmptyString(row.get("artist_display_name")))
                    .put("link_resource", JsonHelper.replaceNullWithEmptyString(row.get("link_resource")))
            );
        }
        return array.toString();
    }
}
