package met.service;

public interface MetBigQueryService {

    public String getListFromMet(String count) throws Exception;
	
	 public String getCustomList(String count) throws Exception;

}
