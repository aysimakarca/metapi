package met;

import met.service.MetBigQueryService;
import met.visionapi.VisionService;
import met.visionapi.VisionService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.api.gax.paging.Page;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

@RestController
public class MainController {

    @Autowired
    MetBigQueryService remoteService;
    
    @Autowired
    VisionService visionService;

    private final static Logger LOG = LoggerFactory.getLogger( MainController.class );

    @RequestMapping("/listMet")
    public String list(@RequestParam(value="count") String count) {
        String response = "";
        try {
             response = remoteService.getListFromMet(count);
        }catch(Exception e){
            LOG.error(e.getMessage());
        }
        return response;
    }
	
	@RequestMapping("/list")
    public String listCustom(@RequestParam(value="count") String count) {
		authImplicit();
        String response = "";
        try {
             response = remoteService.getCustomList(count);
        }catch(Exception e){
            LOG.error(e.getMessage());
        }
        return response;
    }
	
	@RequestMapping("/vision")
    public String listCustom(@RequestParam("image") MultipartFile imagefile) {
        String response = "";
        try {
        	response = visionService.getResult(imagefile);
        }catch(Exception e){
            LOG.error(e.getMessage());
        }
        return response;
    }
	
	static void authImplicit() {
		  // If you don't specify credentials when constructing the client, the client library will
		  // look for credentials via the environment variable GOOGLE_APPLICATION_CREDENTIALS.
		  Storage storage = StorageOptions.getDefaultInstance().getService();

		  System.out.println("Buckets:");
		  Page<Bucket> buckets = storage.list();
		  for (Bucket bucket : buckets.iterateAll()) {
		    System.out.println(bucket.toString());
		  }
		}
}
