package met.util;

import com.google.cloud.bigquery.FieldValue;

public  class JsonHelper {

    public static String replaceNullWithEmptyString(FieldValue value){
        if(value == null || value.getValue() == null){
            return "";
        }
        return value.getStringValue();
    }
}
